<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Foundation\Traits\ApiResponseTrait;
use App\Http\Requests\OrderRequest;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\OrderResource;
use App\Models\Order;

class OrderController extends Controller
{
    use ApiResponseTrait;

    public function store(OrderRequest $request){
        return DB::transaction(function() use($request){
            $data = $request->validated();
            $order = Order::create($data);
            $order->items()->attach($data['items']);
            $order->load('items');
            return $this->successResponse(new OrderResource($order), message : 'Order Created');
        });
    }
}
