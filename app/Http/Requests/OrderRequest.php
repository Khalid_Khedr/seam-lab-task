<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'type' => 'required|in:dine-in,delivery,takeaway',
            'delivery_fees' => 'required_if:type,==,delivery|numeric',
            'customer_phone' => 'required_if:type,==,delivery|regex:/(01)[0-9]{9}/',
            'customer_name' => 'required_if:type,==,delivery|string|max:200',
            'table_number' => 'required_if:type,==,dine-in|numeric|min:1',
            'service_charge' => 'required_if:type,==,dine-in|numeric',
            'waiter_name' => 'required_if:type,==,dine-in|string|max:200',
            'items' => 'required|array',
            'items.*' => 'required|int'
        ];
    }
}
