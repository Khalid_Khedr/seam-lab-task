<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
    protected $casts = [
        'order_data' => 'array'
    ];

    public function items(){
        return $this->belongsToMany(MenueItem::class, 'order_items', 'order_id', 'item_id');
    }
}
