<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'delivery_fees' => $this->delivery_fees,
            'customer_phone' => $this->customer_phone,
            'customer_name' => $this->customer_name,
            'table_number' => $this->table_number,
            'service_charge' => $this->service_charge,
            'waiter_name' => $this->waiter_name,
            'items' => ItemResource::collection($this->whenLoaded('items')),
        ];
    }
}
