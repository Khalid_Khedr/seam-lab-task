<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->enum('type', ['dine-in', 'delivery', 'takeaway']);
            $table->double('delivery_fees')->nullable();
            $table->string('customer_phone')->nullable();
            $table->string('customer_name', 200)->nullable();
            $table->integer('table_number')->nullable();
            $table->double('service_charge')->nullable();
            $table->string('waiter_name', 200)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
};
