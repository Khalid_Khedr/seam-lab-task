<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('min/positive', [ProblemController::class, 'problemOne']);
Route::get('count/numbers/{start_number}/{end_number}', [ProblemController::class, 'problemTwo']);
Route::get('generate/alpha/numbers/{input_string}', [ProblemController::class, 'problemThree']);
Route::get('steps/moves', [ProblemController::class, 'countMinimunOperationsSteps']);

Route::post('order/create',[OrderController::class, 'store']);

