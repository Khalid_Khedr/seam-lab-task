<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Foundation\Traits\ApiResponseTrait;

class ProblemController extends Controller
{
    use ApiResponseTrait;
    //return the lowest integer positive value does not exist in arr_values ,
    public function problemOne(Request $request){
       try{
        $arr = $request->arr;
        $positive_values = array_filter($arr, function ($v) {
            return $v > 0;
        });
        $arr_filtered = array_values(array_filter($positive_values));
        return $this->successResponse(data:min($arr_filtered) - 1);
       }catch(\Exception $ex){
        return $this->errorResponse(data:null, message: $ex->getMessage());
       }
    }

    public function problemTwo($start_number, $end_number){
        try{
            $count = 0;
            for($i = $start_number; $i <= $end_number; $i ++){
                $strConverter = (string) $i;

                strstr($strConverter, "5") == false ? $count ++: '';
            }
            return $this->successResponse(data:$count);
        }catch(\Exception $ex){
            return $this->errorResponse(data:null, message: $ex->getMessage());
        }
    }

    public function problemThree($input_string){

       try{
        $alpha = ['A', 'B', 'C','D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
                  'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
        $arr_converter = str_split($input_string);
        $sum = 0;
        $count = count($arr_converter);
        for($i = 0; $i < count($arr_converter); $i++){
        $index = array_search($arr_converter[$i], $alpha);
        $count = $count - 1;
        $value = $this->getAlphaCount($count);
        $sum = $sum + (($index + 1) * $value);
        }
        return $this->successResponse(data:$sum);
       }catch(\Exception $ex){
        return $this->errorResponse(data:null, message: $ex->getMessage());
       }
    }

    private function getAlphaCount($count){
        if($count == 0){
            return 1;
        }
        $total_multibly = 1;
        for($i = 0; $i < $count; $i++){
            $total_multibly = $total_multibly * 26;
        }
        return $total_multibly;
    }

    public function countMinimunOperationsSteps($target, $n)
    {
       try{
        // $target = [16, 16, 16];
        // $n = 3;
        $result = 0;
        while (1)
        {
            $zero_count = 0;
            $i = 0;
            for($i = 0; $i < $n; $i++)
            {

                if ($target[$i] & 1)
                    break;
                else if ($target[$i] == 0)
                    $zero_count++;
            }
            if ($zero_count == $n)
                return $result;
            if ($i == $n)
            {
                for ($j = 0; $j < $n; $j++)
                $target[$j] = $target[$j] / 2;
                $result++;
            }

            for ($j = $i; $j < $n; $j++)
            {
                if ($target[$j] & 1)
                {
                    $target[$j]--;
                    $result++;
                }
            }
        }
       }catch(\Exception $ex){
        return $ex->getMessage();
       }
    }
}
